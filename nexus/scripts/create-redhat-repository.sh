#!/bin/sh
#
# This script adds 2 spring repositories to Nexus using curl
#
JBOSS_HOME=/opt/jboss
M2_HOME=$JBOSS_HOME/.m2
NEXUS_URL="http://localhost:8081/nexus"
POST_REPO_SERVICE="/service/local/repositories"
GET_REPO_SERVICE="/service/local/repositories"
PUT_PUBLIC_REPO_SERVICE="/service/local/repo_groups/public"
POST_REPO_URL="$NEXUS_URL$POST_REPO_SERVICE"
GET_REPO_URL="$NEXUS_URL$GET_REPO_SERVICE"
PUT_PUBLIC_REPO_URL="$NEXUS_URL$PUT_PUBLIC_REPO_SERVICE"

echo ""
echo "####################################################################################"
echo "# Checking if Repository [redhat-ga] exists."
echo "####################################################################################"
echo ""
REPOSITORY_ID="redhat-ga"
GET_REDHAT_GA_REPO_URL="${GET_REPO_URL}/${REPOSITORY_ID}"
HTTPCODE=`curl -s -o /dev/null -w "%{http_code}" -H "Accept: application/json" -H "Content-Type: application/json" -X GET -u admin:admin123 $GET_REDHAT_GA_REPO_URL`
echo "HTTP RESPONSE CODE $HTTPCODE"

if [ "${HTTPCODE}" = "404" ];
then
  echo ""
  echo "####################################################################################"
  echo "# Creating Repository ${REPOSITORY_ID}"
  echo "####################################################################################"
  echo ""
  echo "curl -i -H \"Accept: application/json\" -H \"Content-Type: application/json\" -f -v -d \"@redhat-ga.json\" -u admin:admin123 $POST_REPO_URL"
  curl -i -H "Accept: application/json" -H "Content-Type: application/json" -f -v -d "@redhat-ga.json" -u admin:admin123 $POST_REPO_URL
fi

echo ""
echo "####################################################################################"
echo "# Add RedHat Repositories to Public Nexus Repository"
echo "####################################################################################"
echo ""
curl -i -s -o /dev/null -H "Accept: application/json" -H "Content-Type: application/json" -f -X PUT -v -d "@public-repository.json" -u admin:admin123 $PUT_PUBLIC_REPO_URL
echo ""
echo "####################################################################################"
echo "# Done"
echo "####################################################################################"
echo ""
