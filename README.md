# Things to be done after docker-compose
# TODO (Go into detail)

## Download necessary dependencies for BPM BPM Suite

## Gogs
- Clone BPM Suite Repository

## JBoss BPM Suite (Designer)
- Clone Repository from Gogs in the designer

## Jenkins
- Install Maven 3.3.9 in Global Tool Configuration
- Install Plugin: Config File Provider Plugin
  - Configure the maven global /var/jenkins_home/maven-settings.xml

- Create a job for a BPM Suite project and use the binaries to deploy
- Deliver new JBoss BPM Suite artifact from Nexus to JBoss BPM Suite Prod


## Fix Maven Settings.xml
## Maven Snapshot
## Maven Release Plugin
