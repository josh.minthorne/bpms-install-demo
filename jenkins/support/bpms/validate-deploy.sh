#! /bin/bash

GROUPID=$1
ARTIFACTID=$2
VERSION=$3
USER="bpmsAdmin:bpmsuite1!"
COUNT=0;
WAIT=30;
CONTINUE=20;
ART="$GROUPID:$ARTIFACTID:$VERSION";
URL="http://bpms-prod:8080/business-central/rest/deployment/$ART";

# Setup wait time (default 30 seconds)
if [! -z $4 ]; then
  WAIT=$4
fi

# Setup the number of continuations (default 20)
if [! -z $5 ]; then
  CONTINUE=$5
fi

echo "Checking successful deployment.."

while [[ $COUNT -lt $CONTINUE && ! -z "$(curl -sb -v -u $USER -X GET -H 'Content-Type: application/json' $URL | grep 'UNDEPLOYED')" ]]; do
   sleep $WAIT;
   COUNT=$((count + 1));
   echo "attempt: $count";
done;

STATUS=$(curl -sb -v -u $USER -X GET -H 'Content-Type: application/json' $URL | sed 's/.*<status>\(.*\)<\/status>.*/\1/');
if [ $COUNT -eq $CONTINUE ]; then
	echo "Deployment has timed out. This does not mean the deployment has not gone through, it has just taken longer than 30 minutes";
	exit 1;
fi

if [ "$STATUS" == "DEPLOYED" ]; then
	echo "Successful deployment";
    exit 0;
else
	echo "Something went wrong. Here's the status of the deployment: $status";
    exit 1;
fi
