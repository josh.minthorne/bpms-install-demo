#! /bin/bash

USER="bpmsAdmin:bpmsuite1!"
GROUPID=$1
ARTIFACTID=$2
VERSION=$3

STATUSCODE=$(curl -w %{http_code} -s -o /dev/null -v -u $USER -X POST -d '{}' -H 'Content-Type: application/json' "http://bpms-prod:8080/business-central/rest/deployment/$GROUPID:$ARTIFACTID:$VERSION/deploy");
if [ $STATUSCODE -eq 202 ]; then
	echo "SUCESSFUL POST"
else
	exit 1
fi
